using Avalonia;
using Avalonia.Controls.Primitives;
using Avalonia.Media;

namespace TestProjectUser.Views;

public class TemplateConnector : TemplatedControl
{
    public static readonly StyledProperty<string> TextValueProperty = AvaloniaProperty.Register<TemplateConnector, string>(nameof(TextValue));

    public string TextValue
    {
        get => GetValue(TextValueProperty);
        set => SetValue(TextValueProperty, value);
    }

    public static readonly StyledProperty<IBrush> LineColorProperty = AvaloniaProperty.Register<TemplateConnector, IBrush>(nameof(LineColor));

    public IBrush LineColor
    {
        get => GetValue(LineColorProperty);
        set => SetValue(LineColorProperty, value);
    }
}
