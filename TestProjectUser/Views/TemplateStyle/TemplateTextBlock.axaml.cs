using Avalonia;
using Avalonia.Controls.Primitives;

namespace TestProjectUser.Views
{
    public class TemplateTextBlock : TemplatedControl
    {
        public static readonly StyledProperty<string> TitleTextProperty = AvaloniaProperty.Register<TemplateTextBlock,string>(nameof(TitleText));
        public string TitleText
        {
            get => GetValue(TitleTextProperty);
            set => SetValue(TitleTextProperty, value);
        }

        public static readonly StyledProperty<string> TextValueProperty = AvaloniaProperty.Register<TemplateTextBlock,string>(nameof(TextValue));

        public string TextValue
        {
            get => GetValue(TextValueProperty);
            set => SetValue(TextValueProperty, value);
        }
    }
}
