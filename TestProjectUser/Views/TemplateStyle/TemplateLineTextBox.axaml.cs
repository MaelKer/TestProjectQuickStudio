using Avalonia;
using Avalonia.Controls.Primitives;
using Avalonia.Media;

namespace TestProjectUser.Views;

public class TemplateLineTextBox : TemplatedControl
{
    public static readonly StyledProperty<string> TextValueProperty = AvaloniaProperty.Register<TemplateLineTextBox, string>(nameof(TextValue));

    public string TextValue
    {
        get => GetValue(TextValueProperty);
        set => SetValue(TextValueProperty, value);
    }

    public static readonly StyledProperty<string> WatermarkValueProperty = AvaloniaProperty.Register<TemplateLineTextBox, string>(nameof(WatermarkValue));

    public string WatermarkValue
    {
        get => GetValue(WatermarkValueProperty);
        set => SetValue(WatermarkValueProperty, value);
    }

    public static readonly StyledProperty<IBrush> LineColorProperty = AvaloniaProperty.Register<TemplateLineTextBox, IBrush>(nameof(LineColor));

    public IBrush LineColor
    {
        get => GetValue(LineColorProperty);
        set => SetValue(LineColorProperty, value);
    }

    public static readonly StyledProperty<string> BoxValueProperty = AvaloniaProperty.Register<TemplateLineTextBox, string>(nameof(BoxValue));

    public string BoxValue
    {
        get => GetValue(BoxValueProperty);
        set => SetValue(BoxValueProperty, value);
    }

    public static readonly StyledProperty<bool>IsReadOnlyProperty = AvaloniaProperty.Register<TemplateLineTextBox, bool>(nameof(IsReadOnly));

    public bool IsReadOnly
    {
        get => GetValue(IsReadOnlyProperty);
        set => SetValue(IsReadOnlyProperty, value);
    }

}
