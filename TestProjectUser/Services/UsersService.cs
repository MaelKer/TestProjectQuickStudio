﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using TestProjectUser.Models;

namespace TestProjectUser.Services
{
    public class UsersService
    {
        private List<UserItem> _userList { get; set; } = new List<UserItem> {
            new UserItem() { Login = "Maman", AuthenticationType = "Triple", License = "1234a", Role = "Popl" },
            new UserItem() { Login = "Maman", AuthenticationType = "Triple", License = "1234a", Role = "Popl" },
            new UserItem() { Login = "Fils", AuthenticationType = "Triple", License = "1234a", Role = "Popl" },
            new UserItem() { Login = "enfant", AuthenticationType = "Triple", License = "1234a", Role = "Popl" },
            new UserItem() { Login = "malq", AuthenticationType = "Triple", License = "1234a", Role = "Popl" },
            new UserItem() { Login = "Patron", AuthenticationType = "Triple", License = "1234a", Role = "Popl" }
        };

        public List<UserItem> GetUsers() => _userList;
    }
}
