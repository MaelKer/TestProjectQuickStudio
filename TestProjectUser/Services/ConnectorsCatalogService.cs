﻿using System;
using System.Collections.Generic;
using TestProjectUser.Models;

namespace TestProjectUser.Services
{
    public class ConnectorsCatalogService
    {
        private List<CatalogConnectorItem> _catalogConnectorItems { get; set; } = new List<CatalogConnectorItem> {
            new CatalogConnectorItem { Name = "Sage 2000 C", Description="POPOOOPOPPOPOPOPOPO",
                VersionDictionnary = new Dictionary<Version, string>
                {
                    { new Version("12.22"), "Ajout d'un bouton boom" },
                    { new Version("23.12"), "Ajout d'une fenetre boom" },
                    { new Version("11.444"), "Ajout d'un scrollviewer" },
                }},
            new CatalogConnectorItem { Name = "Sage 100 C",Description="ddsdqdB",
                VersionDictionnary = new Dictionary<Version, string>
                {
                    { new Version("225.64") , "Ajout d'un bouton boom" },
                    { new Version("66.62") , "Ajout d'une fenetre boom" },
                    { new Version("799.41") , "Ajout d'un scrollviewer" },
                }},
            new CatalogConnectorItem { Name = "Infineo", Description="Non" 
                ,VersionDictionnary = new Dictionary<Version, string>
                {
                    { new Version("23.0000001"), "Ajout d'un bouton boom" },
                    { new Version("798555.55"), "Ajout d'une fenetre boom" },
                    { new Version("122.2"), "Ajout d'un scrollviewer" },
                }},
            new CatalogConnectorItem { Name = "Excel 365",Description="Effectiv",
               VersionDictionnary = new Dictionary<Version, string>
                {
                    { new Version("123488888.33"), "Ajout d'un bouton boom" },
                    { new Version("2555.0"), "Ajout d'une fenetre boom" },
                    { new Version("669.333"), "Ajout d'un scrollviewer" },
                }},
        };

        public List<CatalogConnectorItem> GetConnnectors() => _catalogConnectorItems;
    }
}
