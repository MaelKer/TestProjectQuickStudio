﻿using System;
using System.Collections.Generic;
using TestProjectUser.Models;

namespace TestProjectUser.Services
{
    public class ConnectorsService
    {
        private List<ConnectorItem> _connectorItems { get; set; } = new List<ConnectorItem> {
            new ConnectorItem { Name = "Sage 2000 C",Provider="Mutlimida", Server="SQL123",IsUpdatable=true, Version=new Version("13.84.941")},
        };

        public List<ConnectorItem> GetConnnectors() => _connectorItems;

        public List<ConnectorItem> AddConnector(ConnectorItem connector)
        {
            _connectorItems.Add(new ConnectorItem { Name = connector.Name, Provider = connector.Provider, Server = connector.Server, IsUpdatable = connector.IsUpdatable, Version = connector.Version });
            return _connectorItems;
        }

        public List<ConnectorItem> RemoveConnector(string connectorName)
        {
            _connectorItems.RemoveAll(c => c.Name == connectorName);
            return _connectorItems;
        }

        public void UpdateConnector(ConnectorItem? connectorItem)
        {
            var connector = _connectorItems.Find(c => c.Name == connectorItem.Name);
            if (connector != null)
            {
                connector.Name = connectorItem.Name;
                connector.Provider = connectorItem.Provider;
                connector.Server = connectorItem.Server;    
                connector.IsUpdatable = connectorItem.IsUpdatable;
                connector.Version = connectorItem.Version;
            }
        }
    }
}
