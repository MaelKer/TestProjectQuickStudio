﻿using Avalonia.Media;

namespace TestProjectUser.Services
{
    public static class BrandingService
    {
        public static string BrandingName { get; set; } = "Sage";

        public static IBrush GetBrandingColor()
        {
            if (BrandingService.BrandingName == "Sage")
            {
                return Brushes.Green;
            }
            else
            {
                return Brushes.Blue;
            }
        }
    }
}
