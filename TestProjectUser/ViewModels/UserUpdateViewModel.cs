﻿using Avalonia.Controls;
using Avalonia.Media;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using TestProjectUser.Models;
using TestProjectUser.Services;

namespace TestProjectUser.ViewModels
{
    public partial class UserUpdateViewModel : ViewModelBase
    {

        [ObservableProperty]
        private IBrush? _brandingColor;

        [ObservableProperty]
        private string? _login;
        [ObservableProperty]
        private string? _license;
        [ObservableProperty]
        private string? _role;
        [ObservableProperty]
        private string? _authenticationType;
        [ObservableProperty]
        private string? _title;
        [ObservableProperty]
        private bool? _loginUpdate;


        private UserItem? _userItem;

        public UserUpdateViewModel()
        {
            //Pour le design
        }

        public UserUpdateViewModel(UserItem? userItem)
        {
            _userItem = userItem;

            if (userItem != null)
            {
                Login = userItem?.Login;
                AuthenticationType = userItem?.AuthenticationType;
                License = userItem?.License;
                Role = userItem?.Role;
                Title = "Modification de " + Login;
                LoginUpdate = true;
            }
            else
            {
                Title = "Ajouter un utilisateur";
                LoginUpdate = false;
            }

            BrandingColor = BrandingService.GetBrandingColor();
        }

        public void Cancel(Window window)
        {
            window.Close();
        }

        public void Confirmed(Window window)
        {
            if (_userItem == null)
            {
                _userItem = new UserItem() { Login = Login!, License = License!, Role = Role!, AuthenticationType = AuthenticationType! };
            }
            else
            {
                _userItem.Login = Login!;
                _userItem.AuthenticationType = AuthenticationType!;
                _userItem.License = License!;
                _userItem.Role = Role!;
            }

            WeakReferenceMessenger.Default.Send(_userItem);
            WeakReferenceMessenger.Default.Cleanup();
            window.Close();
        }
    }
}

