﻿using Avalonia.Media;
using CommunityToolkit.Mvvm.ComponentModel;
using TestProjectUser.Services;

namespace TestProjectUser.ViewModels
{
    public partial class MainWindowViewModel : ViewModelBase
    {

        [ObservableProperty]
        private ViewModelBase _content = default!;

        [ObservableProperty]
        private IBrush _brandingColor;

        [ObservableProperty]
        private string _actualWindow;

        public UserListViewModel? UserList { get; }

        public MainWindowViewModel()
        {
            GoToConnector();

            BrandingColor = BrandingService.GetBrandingColor();
        }
        public void GoToConnector()
        {
            Content = new ConnectorListViewModel(new ConnectorsService());
            ActualWindow = "Connecteur";
        }

        public void GoToUser()
        {
            Content = new UserListViewModel(new UsersService());
            ActualWindow = "Utilisateur";
        }
    }
}
