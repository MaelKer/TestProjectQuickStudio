﻿using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Media;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using TestProjectUser.Models;
using TestProjectUser.Services;
using TestProjectUser.Views;

namespace TestProjectUser.ViewModels
{
    public partial class ConnectorCardViewModel : ViewModelBase
    {
        [ObservableProperty]
        private Version _version;

        [ObservableProperty]
        private string _name;

        [ObservableProperty]
        private string _news;

        public Dictionary<string,string> VersionDictionary = new Dictionary<string, string>();

        [ObservableProperty]
        public List<DicoVersion> _dictionnaire = new List<DicoVersion>();
        
        public string Provider;
        public string Server;

        [ObservableProperty]
        private bool _isUpdatable;

        [ObservableProperty]
        private IBrush _brandingColor;

        private ConnectorsCatalogService _service;

        private ConnectorItem _connectorItem { get; set; }

        public ConnectorCardViewModel(ConnectorItem connectorItem, ConnectorsCatalogService service)
        {
            _service = service;
            _connectorItem = connectorItem;
            Name = connectorItem.Name!;
            Version = connectorItem.Version!;
            Name = connectorItem.Name!;
            Provider = connectorItem.Provider!;
            Server = connectorItem.Server!;
            IsUpdatable = connectorItem.IsUpdatable;
            Dictionnaire = GetInfosVersion(connectorItem.Name);

            BrandingColor = BrandingService.GetBrandingColor();
        }

        public void DialogEditConnector()
        {
            if (Avalonia.Application.Current?.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktopApp)
            {
                var connectorCardWindow = new ConnectorUpdateView
                {
                    DataContext = new ConnectorUpdateViewModel(_connectorItem, new ConnectorsCatalogService())
                };
                if (!WeakReferenceMessenger.Default.IsRegistered<ConnectorItem>(this))
                {
                    WeakReferenceMessenger.Default.Register<ConnectorItem>(this, (recipient, message) =>
                    {
                        EditConnector(message);
                    });
                }
                connectorCardWindow.ShowDialog(desktopApp.MainWindow!);
            }
        }

        private void EditConnector(ConnectorItem connector)
        {
            var message = new MessageProps("EditConnector", connector);
            WeakReferenceMessenger.Default.Send(message);
            WeakReferenceMessenger.Default.Cleanup();
        }

        public void RemoveCard()
        {
            var message = new MessageProps("DeleteConnector", Name);
            WeakReferenceMessenger.Default.Send(message);
            WeakReferenceMessenger.Default.Cleanup();
        }

        private List<DicoVersion> GetInfosVersion(string? name)
        {
            var dic = _service.GetConnnectors().Where(c => c.Name == name).FirstOrDefault().VersionDictionnary;
            var dictionnaire = new List<DicoVersion>();
            foreach (var d in dic)
            {
                dictionnaire.Add(new DicoVersion() { Version = d.Key, ReleaseNote = d.Value });
            }
            return dictionnaire;
        }
    }
}
