﻿using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Media;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using TestProjectUser.Models;
using TestProjectUser.Services;
using TestProjectUser.Views;

namespace TestProjectUser.ViewModels
{
    public partial class UserCardViewModel : ViewModelBase
    {
        [ObservableProperty]
        private string _login;

        [ObservableProperty]
        private string _authenticationType;

        [ObservableProperty]
        private string _license;

        [ObservableProperty]
        private string _role;

        [ObservableProperty]
        private IBrush _brandingColor;

        private UserItem _userItem { get; set; }

        public UserCardViewModel(UserItem userItem)
        {
            if (userItem != null)
            {
                _userItem = userItem;
                Login = userItem.Login!;
                AuthenticationType = userItem.AuthenticationType!;
                License = userItem.License!;
                Role = userItem.Role!;
            }
            BrandingColor = BrandingService.GetBrandingColor();
        }

        public void DialogEditUser()
        {
            if (Avalonia.Application.Current?.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktopApp)
            {
                var UserUpdateWindow = new UserUpdateView
                {
                    DataContext = new UserUpdateViewModel(_userItem)
                };

                WeakReferenceMessenger.Default.Register<UserItem>(this, (recipient, message) =>
                {
                    EditUser(message);
                });
                UserUpdateWindow.ShowDialog(desktopApp.MainWindow!);
            }
        }

        private void EditUser(UserItem user)
        {

            var message = new MessageProps("EditUser", Login);
            WeakReferenceMessenger.Default.Send(message);
            WeakReferenceMessenger.Default.Cleanup();

        }

        public void RemoveCard()
        {
            var message = new MessageProps("DeleteUser", Login);
            WeakReferenceMessenger.Default.Send(message);
            WeakReferenceMessenger.Default.Cleanup();
        }
    }
}
