﻿using Avalonia.Controls;
using Avalonia.Media;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using TestProjectUser.Models;
using TestProjectUser.Services;


namespace TestProjectUser.ViewModels
{
    public partial class ConnectorUpdateViewModel : ViewModelBase
    {
  
        [ObservableProperty]
        private IBrush? _brandingColor;

        [ObservableProperty]
        private string? _name;
        [ObservableProperty]
        private string? _description;
        [ObservableProperty]
        private Version? _version;
        [ObservableProperty]
        private string? _provider;
        [ObservableProperty]
        private string? _server;

        public bool IsUpdatable;

        private CatalogConnectorItem? _selectedConnector;

        private string? _filterText = string.Empty;

        private ConnectorItem? _connectorItem;



        public ConnectorUpdateViewModel(ConnectorItem? connectorItem, ConnectorsCatalogService service)
        {
            _connectorItem = connectorItem;
            if (connectorItem != null)
            {
                Name = connectorItem?.Name;
                Version = connectorItem?.Version;
                Provider = connectorItem?.Provider;
                IsUpdatable = connectorItem.IsUpdatable;   
                //ToDo Description = CatalogConnectorItem?.Description
                //ToDo News = CatalogconnectorItem?.News;
            }

            BrandingColor = BrandingService.GetBrandingColor();

            CatalogConnectorList = service.GetConnnectors();

            GetFilterCatalog();

        }

        public void Cancel(Window window)
        {
            window.Close();
        }

        public void Confirmed(Window window)
        {
            if (_connectorItem == null)
            {
                _connectorItem = new ConnectorItem() { Name = Name, Version = Version, Provider = Provider!, Server=Server, IsUpdatable = IsUpdatable};
            }
            else
            {
                _connectorItem.Name = Name!;
                _connectorItem.Version = Version!;
                _connectorItem.Provider = Provider!;
                _connectorItem.Server = Server!;
                _connectorItem.IsUpdatable = IsUpdatable;
            }

            WeakReferenceMessenger.Default.Send(_connectorItem);
            WeakReferenceMessenger.Default.Cleanup();
            window.Close();
        }

        public List<CatalogConnectorItem> CatalogConnectorList { get; set; }

        public ObservableCollection<CatalogConnectorItem> FilteredConnectorList { get; } = new ObservableCollection<CatalogConnectorItem>();

        public string FilterText
        {
            get => _filterText!;
            set
            {
                SetProperty(ref _filterText, value);
                OnFilterTextChanged();
            }
        }

        private void OnFilterTextChanged()
        {
            GetFilterCatalog();
        }

        private void GetFilterCatalog()
        {
            FilteredConnectorList.Clear();
            foreach (CatalogConnectorItem c in CatalogConnectorList.Where(c => c.Name!.ToUpper().Contains(FilterText.ToUpper())))
                FilteredConnectorList.Add(c);
        }
        public CatalogConnectorItem SelectedConnector
        {
            get => _selectedConnector!;
            set
            {
                SetProperty(ref _selectedConnector, value);
                OnSelectedItemChanged();
            }
        }

        private void OnSelectedItemChanged()
        {
            Name = SelectedConnector.Name;
            Description = SelectedConnector.Description;
            var lastKey = SelectedConnector.VersionDictionnary.Keys.ToList().Last();
            Version = SelectedConnector.VersionDictionnary.Where(d=>d.Key == lastKey).FirstOrDefault().Key;
        }
    }
}
