﻿using Avalonia.Controls.ApplicationLifetimes;
using CommunityToolkit.Mvvm.Messaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TestProjectUser.Models;
using TestProjectUser.Services;
using TestProjectUser.Views;

namespace TestProjectUser.ViewModels
{
    public partial class UserListViewModel : ViewModelBase
    {
        public List<UserItem> UserList { get; set; } = new();

        public ObservableCollection<UserCardViewModel> Users { get; } = new();

        public void DialogAddUser()
        {
            if (Avalonia.Application.Current?.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktopApp)
            {
                var UserUpdateWindow = new UserUpdateView
                {
                    DataContext = new UserUpdateViewModel(null)
                };

                WeakReferenceMessenger.Default.Register<UserItem>(this, (recipient, message) =>
                {
                    AddNewUser(message);
                });
                UserUpdateWindow.ShowDialog(desktopApp.MainWindow!);

            }
        }

        public void AddNewUser(UserItem user)
        {
            Users.Add(new UserCardViewModel(user));
            //UserList.Add(new UserItem() { Login = user.Login, AuthenticationType = user.AuthenticationType , License = user.License , Role = user.Role });
        }

        public UserListViewModel(UsersService service)
        {
            WeakReferenceMessenger.Default.Register<MessageProps>(this, (recipient, message) =>
            {
                if (message.From == "DeleteUser")
                {
                    var messageValue = message.MessageObject as string;
                    UserList.RemoveAll(user => user.Login == messageValue);
                    UpdateCards();
                }
                else if (message.From == "EditUser")
                {
                    UpdateCards();
                }
            });
            UserList = service.GetUsers();
            UpdateCards();
        }

        private void UpdateCards()
        {
            Users.Clear();
            foreach (var item in UserList)
            {
                Users.Add(new UserCardViewModel(item));
            }
        }
    }
}
