﻿using Avalonia.Controls.ApplicationLifetimes;
using CommunityToolkit.Mvvm.Messaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TestProjectUser.Models;
using TestProjectUser.Views;
using TestProjectUser.Services;

namespace TestProjectUser.ViewModels
{
    public partial class ConnectorListViewModel : ViewModelBase
    {
        public List<ConnectorItem> ConnectorList { get; set; } = new();

        public ObservableCollection<ConnectorCardViewModel> Connectors { get; } = new();

        private ConnectorsService _service;

        public ConnectorListViewModel(ConnectorsService service)
        {
           _service = service;

            WeakReferenceMessenger.Default.Register<MessageProps>(this, (recipient, message) =>
            {
                if (message.From == "DeleteConnector")
                {
                    var messageValue = message.MessageObject as string;
                    _service.RemoveConnector(messageValue);
                    UpdateCards();
                }
                else if (message.From == "EditConnector")
                {
                    service.UpdateConnector(message.MessageObject as ConnectorItem);
                    UpdateCards();
                }
            });
            UpdateCards();
        }

        public void DialogAddConnector()
        {
            if (Avalonia.Application.Current?.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktopApp)
            {
                var ConnectorUpdateWindow = new ConnectorUpdateView
                {
                    DataContext = new ConnectorUpdateViewModel(null,new ConnectorsCatalogService())
                };
                if (!WeakReferenceMessenger.Default.IsRegistered<ConnectorItem>(this))
                {
                    WeakReferenceMessenger.Default.Register<ConnectorItem>(this, (recipient, message) =>
                    {
                        AddNewConnector(message);
                    });
                }
                ConnectorUpdateWindow.ShowDialog(desktopApp.MainWindow!);
            }
        }

        public void AddNewConnector(ConnectorItem connector)
        {
            ConnectorList = _service.AddConnector(connector);
            UpdateCards();
        }

        private void UpdateCards()
        {
            Connectors.Clear();
            ConnectorList = _service.GetConnnectors();
            foreach (var item in ConnectorList)
            {
                Connectors.Add(new ConnectorCardViewModel(item, new ConnectorsCatalogService()));
            }
        }
    }
}

