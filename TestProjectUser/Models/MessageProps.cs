﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProjectUser.Models
{
    internal class MessageProps
    {
        public MessageProps(string from, object messageObject)
        {
            From = from;
            MessageObject = messageObject;
        }

        public string From { get; set; }
        public object MessageObject { get; set; }


    }
}
