﻿using System;

namespace TestProjectUser.Models
{
    public class DicoVersion
    {
        public Version Version { get; set; }
        public string ReleaseNote { get; set; }
    }
}