﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProjectUser.Models
{
    public class CatalogConnectorItem
    {
         public string? Name { get; set; }
        public string? Description { get; set; }
        public Dictionary<Version, string> VersionDictionnary { get; set; }

    }
}
