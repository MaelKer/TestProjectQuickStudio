﻿using CommunityToolkit.Mvvm.ComponentModel;

namespace TestProjectUser.Models
{
    public class UserItem
    { 


        public string? Login;

        public string? AuthenticationType;

        public string? License;

        public string? Role;

    }
}
