﻿using System;

namespace TestProjectUser.Models
{
    public class ConnectorItem
    {
        public string? Name { get; set; }
        public Version Version { get; set; }
        public string? Provider { get; set; }
        public string? Server { get; set; }
        public bool IsUpdatable { get; set; }

    }
}
